import { createWebHistory, createRouter, Router } from 'vue-router'
import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'tasks',
    component: () => import('./views/Tasks/TaskList.vue')
  }
]

const router: Router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
