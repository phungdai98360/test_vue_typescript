export interface ITaskInfo {
    id?: string
    name?: string
    description?: string
    dueTime?: string
    priority?: string
    isUpdate?: boolean
}

export class TaskInfo {
    id?: string
    name?: string
    description?: string
    dueTime?: string
    priority?: string
    isUpdate: boolean | undefined = false

    constructor(data: ITaskInfo) {
      this.id = data.id || ''
      this.name = data.name || ''
      this.description = data.description || ''
      this.dueTime = data.dueTime || ''
      this.priority = data.priority || ''
      this.isUpdate = data.isUpdate || false
    }
}

export interface IPriorityInfo {
    id: string
    label: string
}

export class PriorityInfo {
    id: string = ''
    label: string = ''
    
    constructor(data: IPriorityInfo) {
      this.id = data.id
      this.label = data.label
    }
}
