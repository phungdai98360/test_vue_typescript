export const PRIORITY: Record<string, string> = {
  low: 'low',
  normal: 'normal',
  high: 'high'
}