import { Options, Vue } from 'vue-property-decorator'
import { ITaskInfo, TaskInfo } from '@/services/TaskService/Enities/TaskEntity'
import ManageTask from './Manage/ManageTask.vue'
import { cloneDeep } from 'lodash'
import Swal, { SweetAlertResult } from 'sweetalert2'

@Options({
  name: 'TaskList',
  components: { ManageTask }
})
export default class TaskList extends Vue {
  private tasks: ITaskInfo[] = []

  mounted(): void {
    const taskString: string | null = localStorage.getItem('tasks') ? localStorage.getItem('tasks') : ''
    this.tasks = (taskString ? JSON.parse(taskString) : []).map((item: Record<string, string>) => new TaskInfo(item))

  }

  onCreate(): void {
    if (this.$refs.manageTask)
    {
      //@ts-ignore
      this.$refs.manageTask.create()
    }
  }

  onUpdate(taskInfo: ITaskInfo): void {
    if (this.$refs.manageTask)
    {
      //@ts-ignore
      this.$refs.manageTask.update(taskInfo)
    }
  }

  createdTask(taskInfo: ITaskInfo): void {
    this.tasks.push(taskInfo)
    localStorage.setItem('tasks', JSON.stringify(this.tasks))
  }
  
  updatedTask(taskInfo: ITaskInfo): void {
    const tasksTemp: ITaskInfo[] = cloneDeep(this.tasks)
    const index: number = tasksTemp.findIndex((item: ITaskInfo) => item.id === taskInfo.id)
    if (index >= 0) {
      tasksTemp[index] = taskInfo
      this.tasks = cloneDeep(tasksTemp)
      localStorage.setItem('tasks', JSON.stringify(this.tasks))
    }
  }

  deleteTask(taskInfo: ITaskInfo): void {
    Swal.fire({
      title: 'Delete task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: SweetAlertResult) => {
      if (result.isConfirmed) {
        const index: number = this.tasks.findIndex((item: ITaskInfo) => item.id === taskInfo.id)
        if (index >= 0) {
          this.tasks.splice(index, 1)
          localStorage.setItem('tasks', JSON.stringify(this.tasks))
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Delete task success',
            showConfirmButton: false,
            timer: 1500
          })
        }
      }
    })
  }
}
