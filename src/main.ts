import { createApp } from 'vue'
import App from '@/App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import router from '@/router'
import jquery from 'jquery'

window.$ = jquery

declare global {
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/naming-convention
    interface Window {
        jQuery: typeof jquery
        $: typeof jquery
        __VUE_DEVTOOLS_GLOBAL_HOOK__: {Vue: App} | null
    }
}

createApp(App).use(router).mount('#app')
